﻿package ik.code.assets
{
	
	public interface IAssetsLoader 
	{

		function set assetsPaths(value:AssetsPaths):void;
		function assetRequest(assetRequest:AssetRequest):void;

	}
	
}
