﻿package ik.code.assets
{
	import flash.media.Sound;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.net.URLRequest;
	import flash.display.Loader;
	import flash.display.Bitmap;
	
	public class CoreAssetsLoader implements IAssetsLoader
	{
		protected var paths:AssetsPaths;
		protected var sound:Sound;
		protected var image:Bitmap;
		protected var loader:Loader;
		protected var requestQueue:Vector.<AssetRequest>;
		protected var callback:Function;
		protected var _isOccupied:Boolean;
		
		public function CoreAssetsLoader() 
		{
			requestQueue = new <AssetRequest>[];
		}
		public function assetRequest(assetRequest:AssetRequest):void
		{
			if(_isOccupied)
			{
				requestQueue.push(assetRequest);
				return;
			}
			
			_isOccupied = true;
			var assetPath:String = getAssetPath(assetRequest);
			
			callback = assetRequest.callback;
			
			switch(assetRequest.type)
			{
				case AssetType.SOUND:
					sound = new Sound();
					sound.addEventListener(Event.COMPLETE, soundLoaded);
					sound.addEventListener(IOErrorEvent.IO_ERROR, soundLoadError);
					sound.load(new URLRequest(assetPath));
					break;
				case AssetType.IMAGE:
					loader = new Loader();
					loader.load(new URLRequest(assetPath));
					loader.contentLoaderInfo.addEventListener(Event.COMPLETE, imageLoaded);
					loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, loadError);
					break;
				case AssetType.VIDEO:
					callback(assetPath);
					checkQueueAndRelease();
					break;
			}
		}
		protected function getAssetPath(request:AssetRequest):String
		{
			switch(request.type)
			{
				case AssetType.SOUND:
					return paths.sounds ? paths.sounds + request.name:paths.main + request.name;
				case AssetType.IMAGE:
					return paths.images ? paths.images + request.name:paths.main + request.name;
				case AssetType.VIDEO:
					return paths.videos ? paths.videos + request.name:paths.main + request.name;
			}
			return "";
		}
		protected function loadError(e:IOErrorEvent):void
		{
			checkQueueAndRelease();
		}
		protected function soundLoadError(e:IOErrorEvent):void
		{
			callback(null);
			checkQueueAndRelease();
		}
		protected function imageLoaded(e:Event):void
		{
			callback(Bitmap(loader.content));
			
			loader.contentLoaderInfo.addEventListener(Event.COMPLETE, imageLoaded);
			loader.unload();
			loader = null;
			
			checkQueueAndRelease();
		}
		protected function soundLoaded(e:Event):void
		{
			callback(sound);
			sound.removeEventListener(Event.COMPLETE, soundLoaded);
			sound = null;
			checkQueueAndRelease();
		}
		protected function checkQueueAndRelease():void
		{
			_isOccupied = false;
			
			if(requestQueue.length)
				assetRequest(requestQueue.shift());
				
		}
		public function set assetsPaths(value:AssetsPaths):void
		{
			paths = value;
		}

	}
	
}
