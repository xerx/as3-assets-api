﻿package ik.code.assets
{
	import ik.code.messages.IMessageEnabled;
	import flash.media.SoundChannel;
	import flash.media.SoundTransform;
	import flash.media.Sound;
	import ik.code.messages.MessageCenter;
	import ik.code.messages.Message;
	import flash.events.Event;

	public class SoundAsset implements IMessageEnabled
	{
		protected const IS_PLAYING:int = 1;
		protected const IS_STOPPED:int = 2;
		protected const IS_PAUSED:int = 3;
		
		protected var messageCenter:MessageCenter;
		protected var channel:SoundChannel;
		protected var sndTransform:SoundTransform;
		protected var state:int;
		protected var pausePosition:Number;
		protected var id:String;
		
		protected var sound:Sound;
		protected var callbackOnComplete:Function;
		
		public var supress:Boolean;
		public var autoPlay:Boolean;
		public var isGlobal:Boolean;
		
		public function SoundAsset(name:String, 
								   supress:Boolean, 
								   autoPlay:Boolean, 
								   callbackOnComplete:Function, 
								   isGlobal:Boolean = false) 
		{
			this.autoPlay = autoPlay;
			this.supress = supress;
			this.isGlobal = isGlobal;
			this.callbackOnComplete = callbackOnComplete;
			
			pausePosition = 0;
			state = IS_STOPPED;
			sndTransform = new SoundTransform(1);
			id = name;
			
			messageCenter = new MessageCenter(this);
			load();
		}
		protected function load():void
		{
			AssetsManager.getAsset(new AssetRequest(id, AssetType.SOUND, soundLoaded, isGlobal));
		}
		protected function soundLoaded(sound:Sound):void
		{			
			this.sound = sound;
			
			if(sound && autoPlay)
				play();
			
			messageCenter.send(Message.COMPLETE, false, sound ? this:null);
		}
		public final function play(startPosition:int = -1, callbackOnComplete:Function = null):void
		{						
			if(!sound || state == IS_PLAYING)
				return;
			
			if(this.callbackOnComplete == null)
				this.callbackOnComplete = callbackOnComplete;
			
			state = IS_PLAYING;
			channel = sound.play(startPosition == -1 ? pausePosition:startPosition);
			channel.soundTransform = sndTransform;
			channel.addEventListener(Event.SOUND_COMPLETE, soundFinishedPlaying);
			
			pausePosition = 0;
		}
		private final function soundFinishedPlaying(e:Event):void
		{
			state = IS_STOPPED;
			channel.removeEventListener(Event.SOUND_COMPLETE, soundFinishedPlaying);
			
			if(callbackOnComplete != null)
				callbackOnComplete();
			else
				messageCenter.send(Message.COMPLETE);
			
			callbackOnComplete = null;
		}
		public final function pause():void
		{
			if(!sound || state != IS_PLAYING)
				return;
			
			state = IS_PAUSED;
			pausePosition = channel.position;
			channel.stop();
		}
		public final function stop():void
		{
			if(!sound || state == IS_STOPPED)
				return;
			
			channel.removeEventListener(Event.SOUND_COMPLETE, soundFinishedPlaying);
			state = IS_STOPPED;
			pausePosition = 0;
			channel.stop();
		}
		public final function set volume(value:Number):void
		{
			sndTransform.volume = value;
			if(channel)
				channel.soundTransform = sndTransform;
		}
		public final function get duration():Number
		{
			return sound ? sound.length:0;
		}
		public final function get position():Number
		{
			return channel ? channel.position:0;
		}
		public final function set position(value:Number):void
		{
			var wasPlaying:Boolean = state == IS_PLAYING;
			
			stop();
			
			if(wasPlaying)
				play(value);
			else
				pausePosition = value;
		}
		public final function get ID():String
		{
			return id;
		}
		public function get msgc():MessageCenter
		{
			return messageCenter;
		}
		public final function destroy():void
		{
			if(channel)
				channel.removeEventListener(Event.SOUND_COMPLETE, soundFinishedPlaying);
			
			channel = null;
			sndTransform = null;
			sound = null;
			state = IS_STOPPED;
			messageCenter.destroy();
			messageCenter = null;
		}
		
		

	}
	
}
