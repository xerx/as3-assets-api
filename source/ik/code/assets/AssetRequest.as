﻿package ik.code.assets
{
	public class AssetRequest 
	{
		
		public var name:String;
		public var type:AssetType;
		public var callback:Function;
		public var isGlobal:Boolean;
		
		public function AssetRequest(name:String, type:AssetType, callback:Function, isGlobal:Boolean = false) 
		{
			this.name = name;
			this.type = type;
			this.callback = callback;
			this.isGlobal = isGlobal;
		}
	}
	
}
