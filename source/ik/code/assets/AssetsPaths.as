﻿package ik.code.assets
{
	public class AssetsPaths 
	{
		public var main:String;
		public var sounds:String;
		public var images:String;
		public var videos:String;
		
		public function AssetsPaths(main:String, sounds:String = null, images:String = null, videos:String = null) 
		{
			this.main = main;
			this.sounds = sounds;
			this.images = images;
			this.videos = videos;
		}

	}
	
}
