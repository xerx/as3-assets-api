﻿package ik.code.assets
{
	public class AssetsManager 
	{
		private static var loader:IAssetsLoader;
		
		public static function init(assetsLoader:IAssetsLoader, assetsPaths:AssetsPaths):void
		{
			loader = assetsLoader;
			loader.assetsPaths = assetsPaths;
		}
		public static function getAsset(request:AssetRequest):void
		{
			loader.assetRequest(request);
		}

	}
	
}
